package com.faluch.app.faluchinfo;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

public class AProposActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apropos);
        String aPropos = "";

       String versionNo = "0.9";
        PackageInfo pInfo = null;
        try{
            pInfo = getPackageManager().getPackageInfo("com.faluch.app.faluchinfo",PackageManager.GET_META_DATA);
        } catch (Resources.NotFoundException e) {
            pInfo = null;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if(pInfo != null) {
            versionNo = pInfo.versionName;
        }

        try {
            aPropos = readFileAsString("aPropos");
        } catch (IOException e) {
            e.printStackTrace();
        }

        TextView mTextView = (TextView) findViewById(R.id.apropos);
        mTextView.setText(aPropos);
        mTextView.append("V : " + versionNo);

    }


    public String readFileAsString(String filePath) throws IOException {


        InputStream input;
        input = getAssets().open(filePath);

        int size = input.available();

        byte[] buffer = new byte[size];
        input.read(buffer);
        input.close();

        String text = new String(buffer);

        return text;

    }

}