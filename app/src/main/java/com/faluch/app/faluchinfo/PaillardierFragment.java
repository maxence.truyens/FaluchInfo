package com.faluch.app.faluchinfo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.faluch.app.entities.Chant;
import com.faluch.app.entities.RepertoireChants;
import com.faluch.app.web.RequetesInterface;
import com.faluch.app.web.RetrofitBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;

/**
 * Created by aymeric on 03/08/2016.
 */

public class PaillardierFragment extends Fragment {

    static public PaillardierFragment INSTANCE = null;
    static private UpdateTask updateTask = null;
    String NOMFICHIER = "NOMFICHIER";
    SwipeRefreshLayout mSwipeRefreshLayout;
    List liste = new ArrayList();
    public ArrayAdapter<String> aa;
    public ArrayAdapter<String> aa2;
    public ListView mListView;
    public ListView mListView2;
    private EditText recherche;
    private Button annuler;


    public PaillardierFragment(){

    }

    public static PaillardierFragment newInstance() {
        PaillardierFragment.INSTANCE = new PaillardierFragment();
        return PaillardierFragment.INSTANCE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.paillardier_layout, container, false);

        TextView tmp = (TextView) rootView.findViewById(R.id.textView);
        tmp.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "BebasNeue.otf"));

        mListView = (ListView) rootView.findViewById(R.id.listView);
        mListView2 = (ListView) rootView.findViewById(R.id.listView2);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                actualiser(getContext());

                //avertie le SwipeRefreshLayout que la mise à jour a été effectuée
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        liste = Chant.getListeChants(this.getContext());

        aa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, liste);

        aa.sort(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });


        mListView.setAdapter(aa);
        mListView2.setAdapter(aa);


        startRefresh(getContext());

        if(liste.isEmpty())
            actualiser(getContext());

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Intent intent = new Intent(getActivity(), AfficheChanson.class);
                //on récupère le nom de la chanson
                intent.putExtra(NOMFICHIER, Chant.toTitreFile(a.getItemAtPosition(position).toString()));
                startActivity(intent);
            }
        });

        mListView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Intent intent = new Intent(getActivity(), AfficheChanson.class);
                //on récupère le nom de la chanson
                intent.putExtra(NOMFICHIER, Chant.toTitreFile(a.getItemAtPosition(position).toString()));
                startActivity(intent);
            }
        });




        recherche = (EditText) rootView.findViewById(R.id.recherche_paillard);

        recherche.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s))
                    startRecherche();
                else
                    startRefresh(getContext());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return rootView;
    }

    public void annulerRecherche(){
        recherche.setText("");
        recherche.setVisibility(View.GONE);
        //annuler.setVisibility(View.GONE);
        mListView2.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        startRefresh(getContext());
    }

    public void startRecherche()
    {
        final String laRecherche;
        List<String> toutesLesChanson;

            laRecherche = recherche.getText().toString();
            toutesLesChanson = Chant.getListeChants(this.getContext());
            liste.clear();
            for(String s: toutesLesChanson)
            {

                if(s.toLowerCase().trim().contains(laRecherche.toLowerCase().trim()))
                liste.add(s);
            }
                aa.clear();

                aa.addAll(liste);

                aa.sort(new Comparator<String>() {
                    @Override
                    public int compare(String lhs, String rhs) {
                        return lhs.compareTo(rhs);
                    }
                });

                aa.notifyDataSetChanged();



    }


    //Rend les option de recherche visible
    public static void AfficheRechercheOption()
    {
        PaillardierFragment.INSTANCE.setRechercheVisible();
    }

    public void setRechercheVisible()
    {
        recherche.setVisibility(View.VISIBLE);
        //annuler.setVisibility(View.VISIBLE);
        mListView2.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
    }


    public void actualiser(Context context)
    {
        updateTask = new UpdateTask(context);
        updateTask.execute((Void) null);

    }

    public void startRefresh(Context context)
    {
        liste.clear();
        liste = Chant.getListeChants(context);

        //annonce à l'adapter que les données ont changé
        aa.clear();
        aa.addAll(liste);
        aa.sort(new Comparator<String>() {
                @Override
                public int compare(String lhs, String rhs) {
                    return lhs.compareTo(rhs);
                }
            });
        aa.notifyDataSetChanged();
        aa.notifyDataSetChanged();

    }




    public class UpdateTask extends AsyncTask<Void, Void, Boolean> {
        private Context context;
        private RepertoireChants rep;

        public  UpdateTask(Context c) {
            this.context = c;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            RequetesInterface requetesInterface = RetrofitBuilder.getComplexClient(this.context);

            Call<RepertoireChants> reqUserCall = requetesInterface.downloadChants(RepertoireChants.getIDChants(this.context), RepertoireChants.getDatesChants(this.context));

            boolean result = false;

            try {
                rep = reqUserCall.execute().body();
                result = rep.saveAll(this.context);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            updateTask = null;
/*
            Fragment current = getFragmentManager().findFragmentById(R.id.FRAG);
            if(current != null)
            {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(current);
                ft.attach(current);
                ft.commit();
            }*/


            if (success) {


                liste.clear();
                liste = Chant.getListeChants(context);
                //annonce à l'adapter que les données ont changé

                if (aa != null)
                {
                    aa.clear();
                    aa.addAll(liste);
                    aa.sort(new Comparator<String>() {
                        @Override
                        public int compare(String lhs, String rhs) {
                            return lhs.compareTo(rhs);
                        }
                    });
                    aa.notifyDataSetChanged();
                }

                Toast.makeText(this.context, rep.saved+" chants ajouté(s).", Toast.LENGTH_SHORT).show();


            } else {
                Toast.makeText(this.context, "Echec de la mise à jour.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            updateTask = null;
            Toast.makeText(this.context, "Interruption de la mise à jour.", Toast.LENGTH_SHORT).show();
        }
    }


}