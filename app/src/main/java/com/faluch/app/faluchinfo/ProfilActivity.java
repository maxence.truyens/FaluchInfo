package com.faluch.app.faluchinfo;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.faluch.app.entities.User;

public class ProfilActivity extends AppCompatActivity {


    private TextView titre;
    private TextView nom;
    private TextView prenom;
    private TextView surnom;
    private TextView ville;
    private TextView mail;
    private TextView date;
    private TextView parrainSurnom;
    private TextView parrainVille;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        titre = (TextView) findViewById(R.id.vosinfos);
        nom = (TextView) findViewById(R.id.profil_nom);
        prenom = (TextView) findViewById(R.id.profil_prenom);
        surnom = (TextView) findViewById(R.id.profil_surnom);
        ville = (TextView) findViewById(R.id.profil_ville);
        mail = (TextView) findViewById(R.id.profil_mail);
        date = (TextView) findViewById(R.id.profil_date);


        /*
        parrainSurnom = (TextView) findViewById(R.id.profil_parrain_surnom);
        parrainVille = (TextView) findViewById(R.id.profil_parrain_ville);
        */

        titre.setTypeface(Typeface.createFromAsset(this.getAssets(), "BebasNeue.otf"));


        User user = User.getUserWithPref(this);

        nom.append(user.getNom());
        prenom.append(user.getPrenom());
        surnom.append(user.getSurnom());
        ville.append(user.getVille());
        mail.append(user.getMail());
        date.append(user.getInscription());

    }
}
