package com.faluch.app.faluchinfo;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.faluch.app.entities.Chant;

public class AfficheChanson extends AppCompatActivity {


    String NOMFICHIER= "NOMFICHIER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_affiche_chanson);
        TextView mTextView = (TextView) findViewById(R.id.chanson);
        TextView titreTexte = (TextView) findViewById(R.id.TitreChanson);
        Intent intent = getIntent();

        Chant c = new Chant();
        c.open(intent.getStringExtra(NOMFICHIER), this);

        titreTexte.setText(c.getTitre());
        titreTexte.setTypeface(Typeface.createFromAsset(getAssets(), "BebasNeue.otf"));
        mTextView.setText(c.getParoles());
    }
}