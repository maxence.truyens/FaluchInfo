package com.faluch.app.faluchinfo;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by aymeric on 04/08/2016.
 */


public class VilleFragment extends Fragment {

    String NOMFICHIER = " ";

    static public VilleFragment INSTANCE = null;
    private ListView mListView;
    private List liste = new ArrayList();
    private ArrayAdapter<String> aa;
    private EditText recherche;
    private Button annuler;
    private String[] toutesLesVilles;


    public VilleFragment() {

    }

    public static VilleFragment newInstance() {

        VilleFragment.INSTANCE = new VilleFragment();
        return VilleFragment.INSTANCE;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.villes_layout, container, false);

        // Lien vers le titre de la tab.
        TextView titreFrag = (TextView) rootView.findViewById(R.id.textView4);
        // Affectation d'une nouvelle police.
        titreFrag.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "BebasNeue.otf"));

        // Lien vers la ListView
        mListView = (ListView) rootView.findViewById(R.id.VilleListeView);
        // On récupère un tableau contenant la liste des villes
        toutesLesVilles = getResources().getStringArray(R.array.listeVilles);
        Collections.addAll(liste, toutesLesVilles);

        aa = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, liste);
        mListView.setAdapter(aa);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                //on récupère le nom de la ville
                String VilleChoisie = a.getItemAtPosition(position).toString();

                VilleChoisie = toTitreFile(VilleChoisie);

                Intent intent = new Intent(getActivity(), AfficheVille.class);
                intent.putExtra(NOMFICHIER, VilleChoisie);
                startActivity(intent);
            }
        });




        recherche = (EditText) rootView.findViewById(R.id.recherche_ville);

        recherche.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s))
                    startRecherche();
                else
                    startRefresh();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        return rootView;
    }

    public void annulerRecherche(){
        recherche.setText("");
        recherche.setVisibility(View.GONE);
        //annuler.setVisibility(View.GONE);
        startRefresh();
    }

    public void startRecherche()
    {
        final String laRecherche;

        laRecherche = recherche.getText().toString();
        List<String> villeTrouve = new ArrayList();

        for(String s: toutesLesVilles)
        {

            if(s.toLowerCase().trim().contains(laRecherche.toLowerCase().trim()))
                villeTrouve.add(s);
        }

        aa.clear();
        aa.addAll(villeTrouve);
        aa.sort(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });
        aa.notifyDataSetChanged();

    }

    private void startRefresh()
    {
        aa.clear();
        aa.addAll(toutesLesVilles);
        aa.sort(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });
        aa.notifyDataSetChanged();
    }

    //Rend les option de recherche visible
    public static void AfficheRechercheOption()
    {
        VilleFragment.INSTANCE.setRechercheVisible();
    }

    public void setRechercheVisible()
    {
        recherche.setVisibility(View.VISIBLE);
        //annuler.setVisibility(View.VISIBLE);
    }

    String toTitreFile(String file)
    {
        String nomFichier = "";

        for(char c : file.toCharArray())
        {
            switch(c)
            {
                case ' ':
                    break;

                case 'é' : nomFichier += 'e';
                    break;

                case 'è' : nomFichier += 'e';
                    break;

                case 'ê' : nomFichier += 'e';
                    break;

                case 'à': nomFichier +=  'a';
                    break;

                case 'â' : nomFichier += 'a';
                    break;

                case 'ù' : nomFichier += 'u';
                    break;

                case 'ç' : nomFichier += 'c';
                    break;

                case 'ï': nomFichier += 'i';
                    break;

                case 'î': nomFichier += 'i';
                    break;

                case 'û': nomFichier += 'u';
                    break;

                case '’':
                    break;

                case '-':
                    break;

                default : nomFichier += c;
                    break;
            }
        }
        return nomFichier;
    }
}