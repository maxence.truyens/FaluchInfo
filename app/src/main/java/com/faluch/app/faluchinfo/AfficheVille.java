package com.faluch.app.faluchinfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by aymeric on 03/08/2016.
 */

/*
    Affichage des infos des villes, lue dans un fichier assets
 */

public class AfficheVille extends AppCompatActivity {

    String NOMFICHIER = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_ville);
        Intent intent= getIntent();
        TextView infosVille = (TextView) findViewById(R.id.villes);
        String nomVille = intent.getStringExtra(NOMFICHIER);
        try {
            infosVille.setText(readFileAsString(nomVille));
        } catch (IOException e) {
            e.printStackTrace();
        }

        nomVille += "Plus";

        TextView VillePlus = (TextView) findViewById(R.id.Plusvilles);
        try {
            VillePlus.setText(readFileAsString(nomVille));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(VillePlus.getText().length() != 0)
        {
            // "Dessiner" le trait de pixel...
            View myLine = (View) findViewById(R.id.line);
            myLine.setVisibility(View.VISIBLE);
        }

    }

    public String readFileAsString(String filePath) throws IOException {


        InputStream input;
        input = getAssets().open(filePath);

        int size=input.available();

        byte[] buffer=new byte[size];
        input.read(buffer);
        input.close();

        String text=new String(buffer);

        return text;

    }
}
