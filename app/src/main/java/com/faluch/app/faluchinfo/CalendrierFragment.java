package com.faluch.app.faluchinfo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by aymeric on 04/08/2016.
 */


/*
 * Cette partie n'est pas encore implémentée
 */

public class CalendrierFragment extends Fragment {

    public static CalendrierFragment INSTANCE = null;

    public CalendrierFragment() {
    }

    public static CalendrierFragment newInstance() {

        CalendrierFragment.INSTANCE = new CalendrierFragment();
        return CalendrierFragment.INSTANCE;
        }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.calendrier_layout, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.Calendrier);
        textView.setText("Calendrier\n\nIci bientôt tu retrouveras un calendrier avec des infos sur les soirées à venir !!!");

        return rootView;
    }
}
