package com.faluch.app.faluchinfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.faluch.app.entities.User;


/**
 * Created by aymeric on 03/08/2016.
 */

public class TabActivity extends AppCompatActivity {


    String BOOLEAN_ACCEPTED = "";
    boolean isAccepted = false;
    int recherche;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_tab);
        Intent intent = getIntent();
        isAccepted = intent.getBooleanExtra(BOOLEAN_ACCEPTED, false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the four
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        if (mViewPager != null) {
            mViewPager.setAdapter(mSectionsPagerAdapter);
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            invalidateOptionsMenu();
                if(recherche == 1){
                    PaillardierFragment.INSTANCE.annulerRecherche();
                    recherche = 0;
                }
                if(recherche == 2){
                    VilleFragment.INSTANCE.annulerRecherche();
                    recherche = 0;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(mViewPager);


            //Ajout de l'icone
            tabLayout.getTabAt(0).setIcon(R.mipmap.ic_launcher);
            //Suppression du texte
            tabLayout.getTabAt(0).setText(null);

            //Ajout de l'icone
            tabLayout.getTabAt(1).setIcon(R.mipmap.codes);
            //Suppression du texte
            tabLayout.getTabAt(1).setText(null);

            tabLayout.getTabAt(2).setIcon(R.mipmap.paillardier);
            //Suppression du texte
            tabLayout.getTabAt(2).setText(null);

            if(isAccepted) {
                //Ajout de l'icone
                tabLayout.getTabAt(3).setIcon(R.mipmap.ville);
                //Suppression du texte
                tabLayout.getTabAt(3).setText(null);

                /*
                 *  Partie Calendrier (pas encore implémenté)
                 */

                //Ajout de l'icone
                //tabLayout.getTabAt(3).setIcon(R.mipmap.calendrier);
                //Suppression du texte
                //tabLayout.getTabAt(3).setText(null);
            }



        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // Item 0 : setting
        // Item 1 : Actualiser
        // Item 2 : Profil
        // Item 3 : A propos
        // Item 4 : Nous contacter
        // Item 5 : Se déconnecter
        // Item 6 : Recherche

        getMenuInflater().inflate(R.menu.menu_tab, menu);

        recherche = 0;
        menu.getItem(6).setIcon(R.mipmap.recherche);
        menu.getItem(6).setTitle(null);

        menu.getItem(0).setVisible(false);
        menu.getItem(3).setVisible(true);
        menu.getItem(5).setVisible(true);


        if(mViewPager.getCurrentItem() == 2) {
            menu.getItem(1).setVisible(true);
            menu.getItem(6).setVisible(true);
        }
        else{
            if(mViewPager.getCurrentItem() == 3)
            {
                menu.getItem(6).setVisible(true);
            }
            else
            {
                menu.getItem(6).setVisible(false);
            }
            menu.getItem(1).setVisible(false);
        }
        if(isAccepted) {
            menu.getItem(4).setVisible(true);
            menu.getItem(2).setVisible(true);
        }else {
            menu.getItem(4).setVisible(false);
            menu.getItem(2).setVisible(false);
        }



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        /**
         * La partie setting n'est pas encore implémentée
         */

        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.action_recherche)
        {

            if(recherche == 0)
            {
                item.setIcon(R.mipmap.annuler_recherche);
                switch(this.mViewPager.getCurrentItem())
                {
                    case 2 : PaillardierFragment.INSTANCE.AfficheRechercheOption();
                        recherche = 1;
                        break;
                    case 3 : VilleFragment.INSTANCE.setRechercheVisible();
                        recherche = 2;
                        break;
                }
            }
            else
            {
                item.setIcon(R.mipmap.recherche);
                switch(this.mViewPager.getCurrentItem())
                {
                    case 2 : PaillardierFragment.INSTANCE.annulerRecherche();
                        recherche = 0;
                        break;
                    case 3 : VilleFragment.INSTANCE.annulerRecherche();
                        recherche = 0;
                        break;
                }
            }



        }

        if(id == R.id.action_actualiser)
        {
            // Renvois le fragment correspondant à la position
            // (0 = AccueilFragment
            //  1 = code de la faluche
            //  2 = Paillardier
            //  3 = Ville
            //  4 = Calendrier)

             PaillardierFragment.INSTANCE.actualiser(this);
        }




        if(id == R.id.action_deconnecter)
        {
            User.disconnectUser(this);
            Intent intent = new Intent(TabActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        if(id == R.id.action_apropos) {
            Intent intent = new Intent(TabActivity.this, AProposActivity.class);
            startActivity(intent);
        }

        if(id == R.id.action_profil) {
            Intent intent = new Intent(TabActivity.this, ProfilActivity.class);
            startActivity(intent);
        }

        if(id == R.id.nous_contacter){
            Intent intent = new Intent(TabActivity.this, ReportFormActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.

                // Renvois le fragment correspondant à la position
                // (0 = AccueilFragment
                //  1 = code de la faluche
                //  2 = Paillardier
                //  3 = Ville
                //  4 = Calendrier)

                switch (position) {
                    case 0:
                        return AccueilFragment.newInstance();

                    case 1:
                        return CodeFalucheFragment.newInstance();

                    case 2:
                        return PaillardierFragment.newInstance();
                    case 3:
                        return VilleFragment.newInstance();
                    //case 4: return CalendrierFragment.newInstance();
                    default:
                        return AccueilFragment.newInstance();
                }
        }

        @Override
        public int getCount() {
            /**
             * Si l'utilisateur à été accepté par son parrain, on renvoie 3 onglets (Le code, le paillardier, et les villes)
             * sinon on n'affiche que les deux premiers.
             */
            if(isAccepted)
                return 4;
            else
                return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Accueil";
                case 1:
                    return "Code de la faluche";
                case 2:
                    return "Paillardier";
                case 3:
                    return "Ville";

                /*
                case 4:
                    return "Calendrier";
                */
            }
            return null;
        }
    }
}

