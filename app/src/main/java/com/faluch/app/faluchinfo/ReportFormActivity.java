package com.faluch.app.faluchinfo;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.faluch.app.entities.Post;
import com.faluch.app.entities.User;
import com.faluch.app.web.RequetesInterface;
import com.faluch.app.web.RetrofitBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;

public class ReportFormActivity extends AppCompatActivity {


    private String objet = "";
    private String corp = "";
    private EditText mEditObjet;
    private EditText mEditCorp;
    private Spinner mSpinCat;
    private Button submit;
    private SendingTask mSendingTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_form);
        mEditObjet = (EditText) findViewById(R.id.objet_mail);
        mEditCorp = (EditText) findViewById(R.id.commentaire);
        mSpinCat = (Spinner) findViewById(R.id.spinnerCat);

        List<String> categories = new ArrayList<String>();
        categories.add("-- Catégorie --");
        categories.add("Paillardier");
        categories.add("Informations de villes");
        categories.add("Suggestion de modifications");
        categories.add("Autre");

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter adapter1 = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, categories);
        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mSpinCat.setAdapter(adapter1);


        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                envoiDonnees();
            }
        });

    }

    private void envoiDonnees() {

        //reset error
        mEditCorp.setError(null);
        mEditObjet.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(mEditCorp.getText().toString())) {
            mEditCorp.setError(getString(R.string.error_field_required));
            focusView = mEditCorp;
            cancel = true;
        }
        if (TextUtils.isEmpty(mEditObjet.getText().toString())) {
            mEditObjet.setError(getString(R.string.error_field_required));
            focusView = mEditObjet;
            cancel = true;
        }

        if (cancel) {
            // There was an error; focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            mSendingTask = new SendingTask(mSpinCat.getSelectedItem().toString(), mEditObjet.getText().toString(), mEditCorp.getText().toString(), this);
            mSendingTask.execute((Void) null);
        }

    }

    public class SendingTask extends AsyncTask<Void, Void, Boolean> {

        String message, sujet, categorie;
        Context context;
        Post post;

        SendingTask(String cat, String objet, String contenu, Context ctx) {
            this.message = contenu;
            this.sujet = objet;
            this.categorie = cat;
            this.context=ctx;
            this.post = new Post();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            RequetesInterface requetesInterface = RetrofitBuilder.getComplexClient(this.context);

            Post.setContext(this.context);
            Call<Post> reqUserCall = requetesInterface.contact(User.getUserWithPref(this.context).getID(),
                    this.sujet, this.categorie, this.message);

            try {
                this.post.setPost(reqUserCall.execute().body());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return !this.post.isFailed();
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mSendingTask = null;

            if (success) {
                Toast.makeText(ReportFormActivity.this, "Message envoyé", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(ReportFormActivity.this, post.getError(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mSendingTask = null;
        }

    }
}



    /*

    private String objet = "";
    private String body = "";
    private int[] choix = {0, 0, 0, 0, 0};
    //choix[]


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_form);
        ArrayList<String> choixSpin1 = setArrayPour1();
        Spinner sp1 = (Spinner) findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter adapter1 = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, choixSpin1);
        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adapter1);


        switch(sp1.getSelectedItemPosition()) {
            case 1: {
                choix[0] = 1;
                Spinner sp2 = (Spinner) findViewById(R.id.spinner2);
                ArrayList<String> choixSpin2 = setArrayPour2();
                ArrayAdapter adapter2 = new ArrayAdapter(this,
                        android.R.layout.simple_spinner_item, choixSpin2);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp2.setAdapter(adapter2);
                sp2.setVisibility(View.VISIBLE);
                sp1.setClickable(false);
            }
            break;
            case 2:{
                choix[0] = 2;
                Spinner sp2 = (Spinner) findViewById(R.id.spinner2);
                ArrayList<String> choixSpin2 = setArrayPour2();
                ArrayAdapter adapter2 = new ArrayAdapter(this,
                        android.R.layout.simple_spinner_item, choixSpin2);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp2.setAdapter(adapter2);
                sp2.setVisibility(View.VISIBLE);
                sp1.setClickable(false);

            }break;
            case 3:{
                choix[0] = 3;
                sp1.setClickable(false);
            }
            break;
        }


    }





    private ArrayList<String> setArrayPour1(){

        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add("-- Que voulez-vous faire? --");
        tmp.add("Reporter un bug");
        tmp.add("Suggerer une amélioration");
        tmp.add("Autres");

        return tmp;
    }

    private ArrayList<String> setArrayPour2(){

        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add("-- Quelle partie? --");
        tmp.add("Code nationnal de la faluche");
        tmp.add("Paillardier");
        tmp.add("Villes");
        tmp.add("Inscription/Connexion");
        tmp.add("Autres");

        return tmp;
    }

    private ArrayList<String> setArrayPourCodeFaluche(){

        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add("-- Quelle article? --");
        tmp.add("Toutes les articles en général");
        tmp.add("Article I");
        tmp.add("Article II");
        tmp.add("Article III");
        tmp.add("Article IV");
        tmp.add("Article V");
        tmp.add("Article VI");
        tmp.add("Article VII");
        tmp.add("Article VIII");
        tmp.add("Article IX");
        tmp.add("Article X");
        tmp.add("Article XI");
        tmp.add("Article XII");
        tmp.add("Article XIII");
        tmp.add("Article XIV");
        tmp.add("Article XV");
        tmp.add("Article XVI");
        tmp.add("Article XVII");
        return tmp;
    }



    private ArrayList<String> setArrayPourAmeliorerPaillardier(){

        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add("-- Que voulez-vous faire? --");
        tmp.add("Proposer un ajout");
        tmp.add("Suggerer une modification");
        tmp.add("Autres");
        return tmp;
    }


    private ArrayList<String> setArrayPourListePaillardier(){

        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add("-- Choisissez une chanson --");
        tmp.add("Toutes les chansons en général");
        tmp.addAll(Chant.getListeChants(this));
        tmp.add("Autres");
        return tmp;
    }


    private ArrayList<String> setArrayPourAmeloirerVille(){

        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add("-- Que voulez-vous faire? --");
        tmp.add("Ajouter une ville non repertoriée");
        tmp.add("Ajouter des infos complémentaires pour une ville");
        tmp.add("Suggerer des modifications pour une ville");
        tmp.add("Autres");
        return tmp;
    }

    private ArrayList<String> setArrayPourListeVille(){

        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add("-- Choisissez une Ville --");
        tmp.add("Toutes les villes");
        Collections.addAll(tmp, getResources().getStringArray(R.array.listeVilles));
        return tmp;
    }


    */


