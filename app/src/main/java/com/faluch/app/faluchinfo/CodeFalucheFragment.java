package com.faluch.app.faluchinfo;

/**
 * Created by aymeric on 04/08/2016.
 */

/*
    Cette partie n'est pas définitive.
 */


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A placeholder fragment containing the code of the faluche.
 */
public class CodeFalucheFragment extends Fragment {

    ListView mListView;

    public CodeFalucheFragment() {
    }


    public static CodeFalucheFragment newInstance() {
        return new CodeFalucheFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.code_faluche, container, false);

        TextView tmp = (TextView) rootView.findViewById(R.id.textView2);
        tmp.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "BebasNeue.otf"));

        ArrayList<HashMap<String, String>> listItem = new ArrayList<>();

        //On déclare la HashMap qui contiendra les informations pour un item
        HashMap<String, String> map;

        //Création d'une HashMap pour insérer les informations du premier item de notre listView
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article I");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part1));

        listItem.add(map);
        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article II");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part2));

        listItem.add(map);
        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article III");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part3));

        listItem.add(map);
        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article IV");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part4));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article V");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part5));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article VI");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part6));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article VII");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part7));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article VIII");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part8));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article IX");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part9));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article X");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part10));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article XI");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part11));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article XII");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part12));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article XIII");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part13));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article XIV");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part14));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article XV");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part15));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article XVI");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part16));

        listItem.add(map);

        //On déclare la HashMap qui contiendra les informations pour un item
        map = new HashMap<>();
        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
        map.put("titre", "Article XVII");
        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
        map.put("description", getResources().getString(R.string.Part17));

        listItem.add(map);

        SimpleAdapter aa = new SimpleAdapter(getActivity(), listItem, R.layout.affichage_liste,
                new String[] {"titre", "description"}, new int[] {R.id.titre, R.id.description});

        mListView = (ListView) rootView.findViewById(R.id.list);
        mListView.setAdapter(aa);



        return rootView;
    }
}
