package com.faluch.app.faluchinfo;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class AccueilFragment extends Fragment {

    static public AccueilFragment INSTANCE = null;
    private TextView Titre;
    private TextView Version;



    public AccueilFragment() {
        // Required empty public constructor
    }


    public static AccueilFragment newInstance() {
        return new AccueilFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_accueil, container, false);
        String versionNo = "1.0";
        PackageInfo pInfo = null;

        try{
            pInfo = this.getActivity().getPackageManager().getPackageInfo("com.faluch.app.faluchinfo", PackageManager.GET_META_DATA);
        } catch (Resources.NotFoundException e) {
            pInfo = null;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if(pInfo != null) {
            versionNo = pInfo.versionName;
        }

        Titre = (TextView) rootView.findViewById(R.id.titre_accueil);
        Version = (TextView) rootView.findViewById(R.id.version);

        Titre.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "BebasNeue.otf"));
        Version.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "BebasNeue.otf"));

        Version.setText("VERSION " + versionNo + " BETA");


        return rootView;
    }
}
