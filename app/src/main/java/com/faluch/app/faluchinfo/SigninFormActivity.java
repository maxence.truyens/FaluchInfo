package com.faluch.app.faluchinfo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.faluch.app.entities.User;
import com.faluch.app.web.RequetesInterface;
import com.faluch.app.web.RetrofitBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;

public class SigninFormActivity extends AppCompatActivity  implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemSelectedListener {

    String BOOLEAN_ACCEPTED = "";

    private UserRegisterTask mAuthTask = null;
    private View mProgressView;
    private View mLoginFormView;

    private EditText mNomView;
    private EditText mPrenomView;
    private EditText mSurnomView;
    private EditText mPasswordView;
    private EditText mConfirmView;
    private EditText mParrainView;
    private EditText mEmailView;
    private Spinner mVilleParrainView;
    private ArrayList<String> arrayParrainVille;

    private Spinner mVilleView;
    private ArrayList<String> arrayVotreVille;


    /*
    mNomView
    mPrenomView
    mSurnomView
    mEmailView
    mPasswordView
    mConfirmView
    mParrainView
    mVilleParrainView
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin_form);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        mNomView = (EditText) findViewById(R.id.nom);
        mPrenomView = (EditText) findViewById(R.id.prenom);
        mSurnomView = (EditText) findViewById(R.id.surnom);
        mEmailView = (EditText) findViewById(R.id.mail);
        mPasswordView = (EditText) findViewById(R.id.mdp);
        mConfirmView = (EditText) findViewById(R.id.mdp2);
        mParrainView = (EditText) findViewById(R.id.parrain);
        mVilleParrainView = (Spinner) findViewById(R.id.spinnerParr);
        mVilleView = (Spinner) findViewById(R.id.spinnerVotre);


        arrayVotreVille = new ArrayList<>();
        arrayVotreVille.add("-- " + getResources().getString(R.string.votre_ville) + " --");
        Collections.addAll(arrayVotreVille, getResources().getStringArray(R.array.listeVilles));

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter adapter1 = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, arrayVotreVille);
        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mVilleView.setAdapter(adapter1);


        arrayParrainVille = new ArrayList<>();
        arrayParrainVille.add("-- " + getResources().getString(R.string.parrain_ville) + " --");
        Collections.addAll(arrayParrainVille, getResources().getStringArray(R.array.listeVilles));

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter adapter2 = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, arrayParrainVille);
        // Specify the layout to use when the list of choices appears
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mVilleParrainView.setAdapter(adapter2);



        Button signinButton = (Button) findViewById(R.id.inscription);
        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSignin();
            }
        });

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void attemptSignin() {
        // Reset errors.
        mNomView.setError(null);
        mPrenomView.setError(null);
        mSurnomView.setError(null);
        mPasswordView.setError(null);
        mConfirmView.setError(null);
        mParrainView.setError(null);
        mEmailView.setError(null);
        /*
        mVilleParrainView.setError(null);
        mVilleView.setError(null);
        */
        // Store values at the time of the login attempt.
        String nom = mNomView.getText().toString();
        String prenom = mPrenomView.getText().toString();
        String password = mPasswordView.getText().toString();
        String confirm = mConfirmView.getText().toString();
        String surnom = mSurnomView.getText().toString();
        String parrain = mParrainView.getText().toString();
        String email = mEmailView.getText().toString();
        String villeParrain = mVilleParrainView.getSelectedItem().toString();
        String ville = mVilleView.getSelectedItem().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if(TextUtils.isEmpty(password))
        {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }else if(!isPasswordValid(password))
        {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }else {
            if(TextUtils.isEmpty(confirm) || !confirm.equals(password))
            {
                mConfirmView.setError(getString(R.string.error_confirm_not_match));
                focusView = mConfirmView;
                cancel = true;
            }
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(nom)) {
            mNomView.setError(getString(R.string.error_field_required));
            focusView = mNomView;
            cancel = true;
        }

        if (TextUtils.isEmpty(prenom)) {
            mPrenomView.setError(getString(R.string.error_field_required));
            focusView = mPrenomView;
            cancel = true;
        }

        if (TextUtils.isEmpty(surnom)) {
            mSurnomView.setError(getString(R.string.error_field_required));
            focusView = mSurnomView;
            cancel = true;
        }

        if (TextUtils.isEmpty(parrain)) {
            mParrainView.setError(getString(R.string.error_field_required));
            focusView = mParrainView;
            cancel = true;
        }

        if (villeParrain.charAt(0) == '-') {
            //mVilleParrainView.setError(getString(R.string.error_field_required));
            focusView = mVilleParrainView;
            cancel = true;
        }

        if (ville.charAt(0) == '-') {
            //mVilleView.setError(getString(R.string.error_field_required));
            focusView = mVilleView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserRegisterTask(nom, prenom, surnom, ville, password, email, parrain, villeParrain, this);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 3;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {

        private final String mNom;
        private final String mPrenom;
        private final String mSurnom;
        private final String mPassword;
        private final String mParrain;
        private final String mEmail;
        private final String mVilleParrain;
        private final String mVille;
        private final Context context;
        User user;

        UserRegisterTask(String nom, String prenom, String surnom, String ville, String password, String email, String parrain, String villeParrain, Context ctx) {
            mNom = nom;
            mPrenom = prenom;
            mSurnom = surnom;
            mParrain = parrain;
            mVilleParrain = villeParrain;
            mVille = ville;

            mEmail = email;
            mPassword = password;
            context=ctx;
            this.user = new User();
            User.setContext(context);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            RequetesInterface requetesInterface = RetrofitBuilder.getComplexClient(this.context);

            Call<User> reqUserCall = requetesInterface.registerUser(mNom,
                    mPrenom,
                    mSurnom,
                    mVille,
                    mPassword,
                    mEmail,
                    mParrain,
                    mVilleParrain);

            try {
                this.user.setUser(reqUserCall.execute().body());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return !this.user.isFailed();
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            if (success && !this.user.isFailed()) {

                new AlertDialog.Builder(context).setMessage("Inscription réussi! Votre parrain à reçu un e-mail pour valider votre inscription (il est possible que cet e-mail se trouve dans les \"SPAM\").\n" +
                        "En attendant pourquoi ne pas utiliser le mode invité ?").setPositiveButton("Menu principal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton("Mode invité", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(SigninFormActivity.this, TabActivity.class);
                        //intent.putExtra(BOOLEAN_ACCEPTED, false);
                        startActivity(intent);
                        finish();
                    }
                }).show();

            } else {
                showProgress(false);
                this.user.afficherErreurConnexion();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }


}
