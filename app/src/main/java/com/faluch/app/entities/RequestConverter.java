package com.faluch.app.entities;

import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;

import java.io.IOException;

import okio.BufferedSink;

/**
 * Created by max on 17/08/16.
 */
public interface RequestConverter {

    void writeRequest(BufferedSink sink) throws IOException;

    void writeJson(JsonWriter jsonW) throws IOException;

    Object readJson(JsonReader reader) throws IOException;
}