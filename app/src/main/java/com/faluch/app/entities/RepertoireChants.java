package com.faluch.app.entities;

import android.content.Context;

import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import okio.BufferedSink;

/**
 * Created by max on 18/08/16.
 */
public class RepertoireChants implements RequestConverter {
    public static ArrayList<Chant> repertoire = new ArrayList<Chant>();
    public ArrayList<Chant> temp = new ArrayList<Chant>();
    public int saved = 0;


    public static void loadChants(Context c) {
        String[] liste =  c.getApplicationContext().getDir(Chant.NAME_DIR, Context.MODE_PRIVATE).list();

        for (String file: liste) {
            Chant chant = new Chant();
            chant.open(file, c);
            if(!contain(chant))
                repertoire.add(chant);
        }
    }

    private static boolean contain(Chant c) {
        for(Chant x: repertoire)
            if (x.getID() == c.getID() && x.getDate().equals(c.getDate()))
                return true;
        return false;
    }

    static public int[] getIDChants(Context c) {
        loadChants(c);
        int[] listefinale = new int[repertoire.size()];
        int i=0;

        for (Chant ch: repertoire) {
            listefinale[i++] = ch.getID();
        }
        return listefinale;
    }

    static public String[] getDatesChants(Context c) {
        loadChants(c);
        String[] listefinale = new String[repertoire.size()];
        int i=0;

        for (Chant ch: repertoire) {
            listefinale[i++] = ch.getDate();
        }
        return listefinale;
    }

    static public String getIDJsonToString(Context c)
    {
        JSONArray jo = new JSONArray();
        for (int i: getIDChants(c) )
            jo.put(i);

        return jo.toString();
    }

    @Override
    public void writeRequest(BufferedSink sink) throws IOException {
        //do the Moshi stuff:
        JsonWriter jsonW = JsonWriter.of(sink);
        jsonW.setIndent("    ");
        writeJson(jsonW);
        // You have to close the JsonWrtiter too (otherwise nothing will happen)
        jsonW.close();
        // Don't forget to close, otherwise nothing appears
        sink.close();

    }

    @Override
    public void writeJson(JsonWriter jsonW) throws IOException {
        jsonW.beginObject();
        for (Chant chant: repertoire) {
            chant.writeJson(jsonW);
        }
        jsonW.endObject();
    }

    @Override
    public RepertoireChants readJson(JsonReader reader) throws IOException {
        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "type":
                    reader.skipValue();
                    break;
                default:
                    Chant c = new Chant();
                    c.fillJson(reader);
                    temp.add(c);
                    break;
            }
        }
        reader.endObject();
        reader.close();
        return this;
    }

    public boolean saveAll(Context c) {
        boolean result = false;

        if(temp.size() == 0)
            result = true;

        for (Chant chant: temp ) {
            boolean save = chant.save(c);
            if(save) {
                result = true;
                saved++;
            }
        }

        loadChants(c);

        return result;
    }
}
