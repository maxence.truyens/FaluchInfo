package com.faluch.app.entities;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.faluch.app.faluchinfo.R;
import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;

import java.io.IOException;

import okio.BufferedSink;

/**
 * Created by max on 06/08/16.
 */

public class User implements RequestConverter {
    private  int id, parrain;
    private String nom, prenom, surnom, mail, password, date_inscription, statut, ville;
    private String error;
    private static Context context;
    private boolean connected;

    public User()
    {
        this.setID(-1);
        this.setNom("inconnu");
        this.setPrenom("inconnu");
        this.setSurnom("inconnu");
        this.setMail("inconnu");
        this.setPass("");
        this.setInscription("0000/00/00 00:00:00");
        this.setStatut("aucun");
        this.setParrain(-1);
        this.setVille("");
        this.setError(null);
    }

    public User(User u)
    {
        this.setUser(u);
    }

    public User(int id, String nom, String prenom, String surnom, String mail, String password, String date_inscription, String statut, String ville, int parrain)
    {
        this.setUser(id, nom, prenom, surnom, mail, password, date_inscription, statut, ville, parrain, null);
    }

    public static boolean isConnected(Context c)
    {
        //TODO Protéger la connexion qui n'est pas secure
        return getUserWithPref(c).isConnected();
    }

    public static User getUserWithPref(Context c)
    {
        SharedPreferences p = getLoginPreference(c);
        User u = new User(p.getInt("id", -1), p.getString("nom", "inconnu"), p.getString("prenom", "inconnu"), p.getString("surnom", "inconnu"),
                p.getString("mail", "inconnu"), p.getString("password", ""), p.getString("date_inscription", "000/00/00 00:00:00"),
                p.getString("statut", "aucun"),  p.getString("ville", ""),p.getInt("parrain", -1));
        u.setConnected(p.getBoolean("connected", false));
        return u;
    }

    public static void registerUserPref(User u, Context c)
    {
        SharedPreferences.Editor edit = getLoginPreference(c).edit();
        edit.putBoolean("connected", true);
        edit.putInt("id", u.getID());
        edit.putString("nom", u.getNom());
        edit.putString("prenom", u.getPrenom());
        edit.putString("surnom", u.getSurnom());
        edit.putString("mail", u.getMail());
        edit.putString("password", u.getPass());
        edit.putString("date_inscription", u.getInscription());
        edit.putString("statut", u.getStatut());
        edit.putString("ville", u.getVille());
        edit.putInt("parrain", u.getParrain());
        edit.apply();
    }

    public static SharedPreferences getLoginPreference(Context c)
    {
        return c.getSharedPreferences("Login", Context.MODE_PRIVATE);
    }

    public static void disconnectUser(Context c)
    {
        getLoginPreference(c).edit().putBoolean("connected", false).apply();
    }
    public static void removeUserPref(Context c)
    {
        getLoginPreference(c).edit().clear().apply();
    }

    public int getID(){ return this.id; }

    public void setID(int s){ this.id=s; }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getNom(){ return this.nom; }

    public void setNom(String s){ this.nom=s; }

    public String getPrenom(){ return this.prenom; }

    public void setPrenom(String s){ this.prenom=s; }

    public String getSurnom(){ return this.surnom; }

    public void setSurnom(String s){ this.surnom=s; }

    public String getMail(){ return this.mail; }

    public void setMail(String s){ this.mail=s; }

    public String getPass(){ return this.password; }

    public void setPass(String s){ this.password=s; }

    public String getInscription(){ return this.date_inscription; }

    public void setInscription(String s){ this.date_inscription=s; }

    public String getStatut(){ return this.statut; }

    public void setStatut(String s){ this.statut=s; }

    public String getVille(){ return this.ville; }

    public void setVille(String s){ this.ville=s; }

    public int getParrain(){ return this.parrain; }

    public void setParrain(int s){ this.parrain=s; }

    public String getError(){ return this.error; }

    public void setError(String s){ this.error=s; }

    public String toString() { return   "ID : "+this.getID()+
            "|Nom : "+this.getNom()+
            "| Prénom : "+this.getPrenom()+
            "| Surnom : "+this.getSurnom()+
            "| Mail : "+this.getMail()+
            "| Password : "+this.getPass()+
            "| Date d'inscription : "+this.getInscription()+
            "| Statut : "+this.getStatut(); }

    public void errorAuth() {
        if(User.context != null)
            this.setError(User.context.getResources().getString(R.string.error_auth_bad_user));
        else
            this.setError("Une erreur est survenue (AUTH).");
    }

    public void errorPost() {
        if(User.context != null)
            this.setError(User.context.getResources().getString(R.string.error_auth_no_post));
        else
            this.setError("Une erreur est survenue (POST).");
    }

    public void errorConfirm() {
        if(User.context != null)
            this.setError(User.context.getResources().getString(R.string.error_auth_no_confirm));
        else
            this.setError("Une erreur est survenue (CONFIRM).");
    }

    public void errorMail() {
        if(User.context != null)
            this.setError(User.context.getResources().getString(R.string.error_auth_mail_used));
        else
            this.setError("Une erreur est survenue (MAIL).");
    }

    public void errorParrain() {
        if(User.context != null)
            this.setError(User.context.getResources().getString(R.string.error_auth_no_parrain));
        else
            this.setError("Une erreur est survenue (PARRAIN).");
    }

    public void errorSurnom() {
        if(User.context != null)
            this.setError(User.context.getResources().getString(R.string.error_auth_surnom_used));
        else
            this.setError("Une erreur est survenue (SURNOM).");
    }

    public void errorInconnue() {
        if(User.context != null)
            this.setError(User.context.getResources().getString(R.string.error_auth_unknown));
        else
            this.setError("Une erreur est survenue (INCONNUE).");
    }

    public boolean isFailed() { return this.error != null; }

    public void setUser(User user)
    {
        setUser(user.getID(), user.getNom(), user.getPrenom(), user.getSurnom(), user.getMail(), user.getPass(), user.getInscription(), user.getStatut(), user.getVille(), user.getParrain(), user.getError());
    }

    public void setUser(int id, String nom, String prenom, String surnom, String mail, String password, String date_inscription, String statut, String ville, int parrain, String err)
    {
        this.setID(id);
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setSurnom(surnom);
        this.setMail(mail);
        this.setPass(password);
        this.setInscription(date_inscription);
        this.setStatut(statut);
        this.setParrain(parrain);
        this.setVille(ville);
        this.setError(err);
    }

    static public void setContext(Context context) {
        User.context = context;
    }

    public boolean isCreated()
    {
        if(!isFailed())
            if(!"inconnu".equals(getNom()) && !"inconnu".equals(getPrenom()) && !"inconnu".equals(getSurnom()) && !"inconnu".equals(getMail()) && !"0000/00/00 00:00:00".equals(getInscription())
                    && !"aucun".equals(getStatut()) && !"".equals(getPass()) && getID() >= 0)
                return true;

        return false;
    }

    public void afficherErreurConnexion()
    {
        if(this.isFailed())
            afficherErreurConnexion(this.error);
    }

    public void afficherErreurConnexion(Object erreur)
    {
        if(erreur instanceof Integer)
            Toast.makeText(context, context.getString((int)erreur), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(context, (String) erreur, Toast.LENGTH_SHORT).show();
    }

    public void registerUserPref(Context c)
    {
        User.registerUserPref(this, c);
    }

    @Override
    public void writeRequest(BufferedSink sink) throws IOException {
        //do the Moshi stuff:
        JsonWriter jsonW = JsonWriter.of(sink);
        jsonW.setIndent("    ");
        writeJson(jsonW);
        // You have to close the JsonWrtiter too (otherwise nothing will happen)
        jsonW.close();
        // Don't forget to close, otherwise nothing appears
        sink.close();
    }

    @Override
    public void writeJson(JsonWriter jsonW) throws IOException {
        jsonW.beginObject();
        jsonW.name("id").value(this.getID());
        jsonW.name("nom").value(this.getNom());
        jsonW.name("prenom").value(this.getPrenom());
        jsonW.name("surnom").value(this.getSurnom());
        jsonW.name("mail").value(this.getMail());
        jsonW.name("password").value(this.getPass());
        jsonW.name("date_inscription").value(this.getInscription());
        jsonW.name("statut").value(this.getStatut());
        jsonW.name("ville").value(this.getVille());
        jsonW.name("parrain").value(this.getParrain());
        jsonW.endObject();
    }

    @Override
    public User readJson(JsonReader reader) throws IOException {
        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "id":
                    this.setID(reader.nextInt()); break;
                case "nom":
                    this.setNom(reader.nextString()); break;
                case "prenom":
                    this.setPrenom(reader.nextString()); break;
                case "surnom":
                    this.setSurnom(reader.nextString()); break;
                case "mail":
                    this.setMail(reader.nextString()); break;
                case "password":
                    this.setPass(reader.nextString()); break;
                case "date_inscription":
                    this.setInscription(reader.nextString()); break;
                case "statut":
                    this.setStatut(reader.nextString()); break;
                case "ville":
                    this.setVille(reader.nextString()); break;
                case "parrain":
                    this.setParrain(reader.nextInt()); break;
                case "badUser":
                    reader.skipValue();
                    this.errorAuth(); break;
                case "badPost":
                    reader.skipValue();
                    this.errorPost(); break;
                case "noConfirm":
                    reader.skipValue();
                    this.errorConfirm(); break;
                case "badMail":
                    reader.skipValue();
                    this.errorMail(); break;
                case "badParrain":
                    reader.skipValue();
                    this.errorParrain(); break;
                case "badSurnom":
                    reader.skipValue();
                    this.errorSurnom(); break;
                case "notAdd":
                    reader.skipValue();
                    this.errorInconnue(); break;

                default: reader.skipValue(); break;
            }
        }
        reader.endObject();
        reader.close();
        return this;
    }

    public boolean isMember()
    {
        return !"Validation".equals(this.getStatut());
    }
}
