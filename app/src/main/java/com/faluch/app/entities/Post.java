package com.faluch.app.entities;

import android.content.Context;

import com.faluch.app.faluchinfo.R;
import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;

import java.io.IOException;

import okio.BufferedSink;

/**
 * Created by max on 27/09/16.
 */
public class Post implements RequestConverter{
    private String date, message, subject, categorie;
    private int idAuthor, id;
    private String error;
    static private Context context;

    public Post() {
        setPost("0000/00/00 00:00:00", "", "", "", -1, -1);
    }

    public Post(String date, String categorie, String subject, String message, int idAuthor, int id) {
        setPost(date, categorie, subject, message, idAuthor, id);
    }

    public void setPost(Post p) {
        setPost(p.date, p.categorie, p.subject, p.message, p.idAuthor, p.id);
    }

    public void setPost(String date, String categorie, String subject, String message, int idAuthor, int id) {
        setID(id);
        setIDAuthor(idAuthor);
        setMessage(message);
        setSubject(subject);
        setCategorie(categorie);
        setDate(date);
        setError(null);
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getIDAuthor() {
        return idAuthor;
    }

    public void setIDAuthor(int idAuthor) {
        this.idAuthor = idAuthor;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    static public void setContext(Context c) {
        Post.context = c;
    }

    static public Context getContext() {
        return Post.context;
    }


    public void errorBadPost() {
        if(Post.context != null)
            this.setError(Post.context.getResources().getString(R.string.error_post_bad_post));
        else
            this.setError("Une erreur est survenue (POST).");
    }

    public void errorBadUser() {
        if(Post.context != null)
            this.setError(Post.context.getResources().getString(R.string.error_post_bad_user));
        else
            this.setError("Une erreur est survenue (USER).");
    }

    public void errorInconnue() {
        if(Post.context != null)
            this.setError(Post.context.getResources().getString(R.string.error_auth_unknown));
        else
            this.setError("Une erreur est survenue (INCONNUE).");
    }

    public boolean isFailed() { return this.error != null; }

    @Override
    public void writeJson(JsonWriter jsonW) throws IOException {
        jsonW.beginObject();
        jsonW.name("id").value(this.getID());
        jsonW.name("id_user").value(this.getIDAuthor());
        jsonW.name("date_envoi").value(this.getDate());
        jsonW.name("message").value(this.getMessage());
        jsonW.name("categorie").value(this.getCategorie());
        jsonW.name("sujet").value(this.getSubject());
        jsonW.endObject();
    }

    @Override
    public Post readJson(JsonReader reader) throws IOException {
        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "id":
                    this.setID(reader.nextInt()); break;
                case "id_user":
                    this.setIDAuthor(reader.nextInt()); break;
                case "date_envoie":
                    this.setDate(reader.nextString()); break;
                case "categorie":
                    this.setCategorie(reader.nextString()); break;
                case "sujet":
                    this.setSubject(reader.nextString()); break;
                case "message":
                    this.setMessage(reader.nextString()); break;
                case "badUser":
                    reader.skipValue();
                    this.errorBadUser(); break;
                case "badPost":
                    reader.skipValue();
                    this.errorBadPost(); break;
                case "type":
                    reader.skipValue(); break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        reader.close();
        return this;
    }

    public void writeRequest(BufferedSink sink) throws IOException {
        //do the Moshi stuff:
        JsonWriter jsonW = JsonWriter.of(sink);
        jsonW.setIndent("    ");
        writeJson(jsonW);
        // You have to close the JsonWrtiter too (otherwise nothing will happen)
        jsonW.close();
        // Don't forget to close, otherwise nothing appears
        sink.close();
    }

}
