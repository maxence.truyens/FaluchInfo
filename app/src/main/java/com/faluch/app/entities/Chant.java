package com.faluch.app.entities;

import android.content.Context;

import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import okio.BufferedSink;

/**
 * Created by max on 14/08/16.
 */
public class Chant implements RequestConverter {
    public static String NAME_DIR = "paillardier";
    private static String DIR = "paillardier/";
    private static String LOCATION = "/src/main/assets/"+DIR;
    private String titre, auteur, paroles, date_modif;
    private int id;

    public Chant()
    {
        this.setID(-1);
        this.setDate("000/00/00 00:00:00");
        this.setTitre("inconnu");
        this.setAuteur("inconnu");
        this.setParoles("aucunes");
    }

    public Chant(int _id, String _date_modif, String _titre, String _auteur, String _paroles)
    {
        this.setID(_id);
        this.setDate(_date_modif);
        this.setTitre(_titre);
        this.setAuteur(_auteur);
        this.setParoles(_paroles);
    }

    public Chant(Chant c)
    {
        this.setID(c.getID());
        this.setDate(c.getDate());
        this.setParoles(c.getParoles());
        this.setTitre(c.getTitre());
        this.setAuteur(c.getAuteur());
    }

    public static String toTitreFile(String file)
    {
        if(file.contains("["))
        file = file.substring(0, file.indexOf("["));
        file = file.replaceAll("[éèê]", "e");
        file = file.replaceAll("['’\\s]", "");
        file = file.replaceAll("[àâ]", "a");
        file = file.replaceAll("ç", "c");
        file = file.replaceAll("[ùû]", "u");
        file = file.replaceAll("ï", "i");

        return file;
    }

    public static List getListeChants(Context c)
    {
        String[] liste =  c.getApplicationContext().getDir(Chant.NAME_DIR, Context.MODE_PRIVATE).list();
        List listefinale = new ArrayList(liste.length);
        int i = 0;

        for (String file: liste) {
            Chant chant = new Chant();
            chant.open(file, c);
            listefinale.add(chant.getPaillardierTitle());
        }
        return listefinale;
    }

    public int getID() { return this.id; }

    public void setID(int i) { this.id = i; }

    public String getDate() { return this.date_modif; }

    public void setDate(String date) { this.date_modif = date; }

    public String getTitre()
    {
        return this.titre;
    }

    public void setTitre(String t)
    {
        this.titre = t;
    }

    public String getAuteur()
    {
        return this.auteur;
    }

    public void setAuteur(String t)
    {
        this.auteur = t;
    }

    public String getParoles()
    {
        return this.paroles;
    }

    public void setParoles(String t)
    {
        this.paroles = t;
    }

    public boolean isComplete()
    {
        return !"aucunes".equals(getParoles()) && !"inconnu".equals(getTitre()) && getID() >= 0;
    }

    public boolean save(Context c) {
        if(this.isComplete()) {
            boolean retour = false;
            File file = new File(c.getApplicationContext().getDir(Chant.NAME_DIR, Context.MODE_PRIVATE), this.toTitreFile());
            OutputStream out = null;
            try {
                out = new BufferedOutputStream(new FileOutputStream(file));
                out.write(convertToFile().getBytes());
                retour = true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return retour;
            }
        }
        return false;
    }

    public String toTitreFile()
    {
        return Chant.toTitreFile(this.getTitre());
    }

    public void open(String filename, Context c)
    {
        XMLDOMParser parser = new XMLDOMParser();
        File file = new File(c.getApplicationContext().getDir(Chant.NAME_DIR, Context.MODE_PRIVATE), filename);
        InputStream out = null;
        try {
            out = new BufferedInputStream(new FileInputStream(file));
            Document doc = parser.getDocument(out);

            // Get elements by name employee
            NodeList id = doc.getElementsByTagName("id");
            NodeList date = doc.getElementsByTagName("date");
            NodeList titre = doc.getElementsByTagName("titre");
            NodeList auteur = doc.getElementsByTagName("auteur");
            NodeList paroles = doc.getElementsByTagName("paroles");

            if(id.getLength() > 0)
                this.setID(Integer.parseInt(id.item(0).getTextContent()));

            if(date.getLength() > 0)
                this.setDate(date.item(0).getTextContent());

            if(titre.getLength() > 0)
                this.setTitre(titre.item(0).getTextContent());

            if(auteur.getLength() > 0)
                this.setAuteur(auteur.item(0).getTextContent());

            if(paroles.getLength() > 0)
                this.setParoles(paroles.item(0).getTextContent());
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getPaillardierTitle()
    {
        //TODO Ajouter l'auteur dans le titre
        String aut = "";
        if(!"inconnu".equals(this.getAuteur().toLowerCase()) && !"".equals(this.getAuteur()))
            aut = " ["+this.getAuteur()+"]";
        return this.getTitre()+aut;
    }

    private String convertToFile()
    {
        return  "<xml><id>"+getID()+"</id><date>"+getDate()+"</date>"+
                "<titre>"+getTitre()+"</titre>"+
                "<auteur>"+getAuteur()+"</auteur>"+
                "<paroles>"+getParoles()+"</paroles></xml>";
    }

    public void writeRequest(BufferedSink sink) throws IOException {
        //do the Moshi stuff:
        JsonWriter jsonW = JsonWriter.of(sink);
        jsonW.setIndent("    ");
        writeJson(jsonW);
        // You have to close the JsonWrtiter too (otherwise nothing will happen)
        jsonW.close();
        // Don't forget to close, otherwise nothing appears
        sink.close();
    }

    @Override
    public void writeJson(JsonWriter jsonW) throws IOException {
        jsonW.beginObject();
        jsonW.name("id").value(this.getID());
        jsonW.name("date_modif").value(this.getDate());
        jsonW.name("titre").value(this.getTitre());
        jsonW.name("auteur").value(this.getAuteur());
        jsonW.name("paroles").value(this.getParoles());
        jsonW.endObject();
    }

    @Override
    public Chant readJson(JsonReader reader) throws IOException {
        fillJson(reader);
        reader.close();
        return this;
    }

    public void fillJson(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "id":
                    this.setID(reader.nextInt()); break;
                case "date_modif":
                    this.setDate(reader.nextString()); break;
                case "titre":
                    this.setTitre(reader.nextString()); break;
                case "auteur":
                    this.setAuteur(reader.nextString()); break;
                case "paroles":
                    this.setParoles(reader.nextString()); break;
                default:break;
            }
        }
        reader.endObject();

    }

    public String toString() { return getID()+"-"+getPaillardierTitle(); }
}