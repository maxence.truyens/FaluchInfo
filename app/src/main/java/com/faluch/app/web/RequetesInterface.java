package com.faluch.app.web;

import com.faluch.app.entities.Post;
import com.faluch.app.entities.RepertoireChants;
import com.faluch.app.entities.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Maxence on 12/08/16.
 */
public interface RequetesInterface {
    @FormUrlEncoded
    @POST("app/connexion.php")
    Call<User> connectUser(@Field("mail") String mail, @Field("pass") String pass);

    @FormUrlEncoded
    @POST("app/chants.php")
    Call<RepertoireChants> downloadChants(@Field("possedes[]") int[] chants, @Field("dates[]") String[] dates);

    @FormUrlEncoded
    @POST("app/inscription.php")
    Call<User> registerUser(@Field("nom") String nom,
                            @Field("prenom") String prenom,
                            @Field("surnom") String surnom,
                            @Field("ville") String ville,
                            @Field("pass") String password,
                            @Field("mail") String email,
                            @Field("parrain") String parrain,
                            @Field("ville_parrain") String villeParrain);

    @FormUrlEncoded
    @POST("app/contact.php")
    Call<Post> contact(@Field("id_user") int idUser,
                       @Field("sujet") String sujet,
                       @Field("categorie") String categorie,
                       @Field("message") String message);
}
