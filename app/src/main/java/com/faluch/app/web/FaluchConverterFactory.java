package com.faluch.app.web;

import com.faluch.app.entities.Chant;
import com.faluch.app.entities.Post;
import com.faluch.app.entities.RepertoireChants;
import com.faluch.app.entities.User;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Created by max on 07/08/16.
 */
public class FaluchConverterFactory extends Converter.Factory{

    public static FaluchConverterFactory create(){
        return new FaluchConverterFactory();
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodsAnnotations, Retrofit retrofit) {
        //If it's a Photo instance then convert
        if(type==User.class )
            return FaluchRequestConverter.INSTANCE;
        else if (type == Chant.class)
            return FaluchRequestConverter.INSTANCE;
        else if (type == Post.class)
            return FaluchRequestConverter.INSTANCE;
        //else use the Chain of responsability pattern and return null
        //the api will look at the next converter
        return null;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[]
            annotations, Retrofit retrofit) {
        //If it's a Photo instance then convert
        if(type==User.class)
            return FaluchResponseConverter.INSTANCE;
        else if (type == RepertoireChants.class)
            return FaluchResponseConverter.INSTANCE;
        else if (type == Post.class)
            return FaluchResponseConverter.INSTANCE;
        //else use the Chain of responsability pattern and return null
        //the api will look at the next converter
        return null;
    }
}
