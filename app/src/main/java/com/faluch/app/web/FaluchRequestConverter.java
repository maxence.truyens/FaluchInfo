package com.faluch.app.web;

import com.faluch.app.entities.Chant;
import com.faluch.app.entities.Post;
import com.faluch.app.entities.User;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import retrofit2.Converter;

/**
 * Created by max on 07/08/16.
 */
public class FaluchRequestConverter<T> implements Converter<T, RequestBody>{

    static final FaluchRequestConverter<User> INSTANCE = new FaluchRequestConverter<User>();
    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");
    private User user = null;
    private Chant chant = null;
    private Post post = null;

    private FaluchRequestConverter() { }

    @Override
    public RequestBody convert(T value) throws IOException {
        // Ensure the right object is passed to you
        if (value instanceof User) {
            user = (User) value;
            return new RequestBody() {
                @Override
                public MediaType contentType() {return MEDIA_TYPE;}
                @Override
                public void writeTo(BufferedSink sink) throws IOException {
                    user.writeRequest(sink); }
            };
        } else if(value instanceof Chant) {
            chant = (Chant) value;
            return new RequestBody() {
                @Override
                public MediaType contentType() {return MEDIA_TYPE;}
                @Override
                public void writeTo(BufferedSink sink) throws IOException {
                    chant.writeRequest(sink); }
            };
        } else if(value instanceof Post) {
            post = (Post) value;
            return new RequestBody() {
                @Override
                public MediaType contentType() {return MEDIA_TYPE;}
                @Override
                public void writeTo(BufferedSink sink) throws IOException {
                    post.writeRequest(sink); }
            };
        } else {throw new IllegalArgumentException();}
    }
}
