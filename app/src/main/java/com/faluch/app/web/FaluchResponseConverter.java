package com.faluch.app.web;

import com.faluch.app.entities.Post;
import com.faluch.app.entities.RepertoireChants;
import com.faluch.app.entities.User;
import com.squareup.moshi.JsonReader;

import java.io.IOException;

import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Converter;

/**
 * Created by max on 08/08/16.
 */
public class FaluchResponseConverter implements Converter<ResponseBody, Object>{
    static final FaluchResponseConverter INSTANCE = new FaluchResponseConverter();

    //default empty constructor
    private FaluchResponseConverter() {
    }

    @Override
    public Object convert(ResponseBody value) throws IOException {
        return selectType(value.source());
    }

    private Object selectType(BufferedSource source) throws IOException {
        if(source==null){throw new IOException("readJSON");}

        JsonReader reader = JsonReader.of(source);
        reader.beginObject();

        while (reader.hasNext()) {
            switch(reader.nextName()) {
                case "type":
                    switch (reader.nextString()) {
                        case "user":
                            return new User().readJson(reader);

                        case "post":
                            return new Post().readJson(reader);

                        case "chant[]":
                            return new RepertoireChants().readJson(reader);

                        default: break;
                    }
                    break;

                default: break;
            }
        }

        reader.endObject();
        reader.close();
        source.close();
        return null;
    }

}