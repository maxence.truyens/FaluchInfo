package com.faluch.app.web;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

/**
 * Created by max on 06/08/16.
 */
public class RetrofitBuilder {
    public static final String BASE_URL = "http://wikiluche.lescigales.org";

    public static RequetesInterface getSimpleClient()
    {
        Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl(BASE_URL)
                                        .build();

        RequetesInterface requetesInterface = retrofit.create(RequetesInterface.class);
        return requetesInterface;
    }

    public static RequetesInterface getComplexClient(Context ctx) {
        // get the OkHttp client
        OkHttpClient client = getOkHttpClient(ctx);
        // now it's using the cach
        // Using my HttpClient
        Retrofit raCustom = new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(FaluchConverterFactory.create())
                .build();
        RequetesInterface webServer = raCustom.create(RequetesInterface.class);
        return webServer;
    }

    @NonNull
    public static OkHttpClient getOkHttpClient(Context ctx) {
        // Define the OkHttp Client with its cache!
        // Assigning a CacheDirectory
        File myCacheDir=new File(ctx.getCacheDir(),"OkHttpCache");
        // You should create it...
        int cacheSize=1024*1024;
        Cache cacheDir=new Cache(myCacheDir,cacheSize);

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.cache(cacheDir);
        builder.networkInterceptors().add(httpLoggingInterceptor);

        return builder.build();
    }
}
